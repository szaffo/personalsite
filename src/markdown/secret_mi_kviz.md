<!-- extends layouts/mdLayout.pug -->
 
<!-- block title -->
<!-- ++title("Backend helper") -->

<!-- block mdContent -->

# Mesterséges intelligencia kvíz megoldások

- [Mesterséges intelligencia kvíz megoldások](#mesterséges-intelligencia-kvíz-megoldások)
  - [2](#2)
    - [1. Hogyan NEM csökkenthető egy állapottér modell bonyolultsága?](#1-hogyan-nem-csökkenthető-egy-állapottér-modell-bonyolultsága)
    - [2. Mitől NEM függ egy reprezentációs gráf bonyolultsága?](#2-mitől-nem-függ-egy-reprezentációs-gráf-bonyolultsága)
    - [3. Melyik NEM része a probléma dekompozíciós modellnek?](#3-melyik-nem-része-a-probléma-dekompozíciós-modellnek)
    - [4. Milyen egy dekompozíciós operátor?](#4-milyen-egy-dekompozíciós-operátor)
    - [5. Az alábbiak közül melyek NEM elemei az állapottér modellnek? (2)](#5-az-alábbiak-közül-melyek-nem-elemei-az-állapottér-modellnek-2)
    - [6. Mely állítások igazak az állapotgráfra az alábbiak közül? (3)](#6-mely-állítások-igazak-az-állapotgráfra-az-alábbiak-közül-3)
    - [7. Az alábbi feladat-modellezések közül melyeknél NEM egyezett meg a problématér a reprezentációs gráf startcsúcsból kivezető útjaival? (2)](#7-az-alábbi-feladat-modellezések-közül-melyeknél-nem-egyezett-meg-a-problématér-a-reprezentációs-gráf-startcsúcsból-kivezető-útjaival-2)
    - [8. Melyik ok-okozati összefüggések igazak az alábbiak közül? (2)](#8-melyik-ok-okozati-összefüggések-igazak-az-alábbiak-közül-2)
    - [9. Hogyan csökkenthető egy állapottér modellben a műveletek kiszámítási bonyolultsága? (2)](#9-hogyan-csökkenthető-egy-állapottér-modellben-a-műveletek-kiszámítási-bonyolultsága-2)
    - [10. Mely fogalmak kapcsolhatók egymáshoz?](#10-mely-fogalmak-kapcsolhatók-egymáshoz)
    - [11. Melyek a feltételei a visszafelé haladó keresésnek?](#11-melyek-a-feltételei-a-visszafelé-haladó-keresésnek)
    - [12. Mi célt szolgál a probléma-redukciós operátor?](#12-mi-célt-szolgál-a-probléma-redukciós-operátor)
  - [3](#3)
    - [1. Az alábbi módszerek közül melyiknél változhat futás közben a globális munkaterület mérete?](#1-az-alábbi-módszerek-közül-melyiknél-változhat-futás-közben-a-globális-munkaterület-mérete)
    - [2. Melyik állítás NEM igaz a lokális keresésekre az alábbiak közül? (2)](#2-melyik-állítás-nem-igaz-a-lokális-keresésekre-az-alábbiak-közül-2)
    - [3. Tekinthető-e a hegymászó módszer a tabu keresés speciális változatának? (2)](#3-tekinthető-e-a-hegymászó-módszer-a-tabu-keresés-speciális-változatának-2)
    - [4. Hány helyen használ a szimulált hűtés algoritmusa véletlenített módszert?](#4-hány-helyen-használ-a-szimulált-hűtés-algoritmusa-véletlenített-módszert)
    - [5. Mely állítások igazak az alábbiak közül? (2)](#5-mely-állítások-igazak-az-alábbiak-közül-2)
    - [6. Melyek az alábbiak közül a tabu keresés hátrányai? (2)](#6-melyek-az-alábbiak-közül-a-tabu-keresés-hátrányai-2)
    - [7. Mely állítások NEM igazak a lokális keresésre az alábbiak közül? (2)](#7-mely-állítások-nem-igazak-a-lokális-keresésre-az-alábbiak-közül-2)
    - [8. Melyek az alábbiak közül a hegymászó módszer hátrányai? (3)](#8-melyek-az-alábbiak-közül-a-hegymászó-módszer-hátrányai-3)
    - [9. Hogyan hat a heurisztika információ tartalma egy kereső rendszer futási idejére? (3)](#9-hogyan-hat-a-heurisztika-információ-tartalma-egy-kereső-rendszer-futási-idejére-3)
    - [10. Mely algoritmusok születtek a hegymászó módszer zsákutcában való beragadásának elkerülésére? (3)](#10-mely-algoritmusok-születtek-a-hegymászó-módszer-zsákutcában-való-beragadásának-elkerülésére-3)
    - [11. Mi a lokális keresések általános vezérlési stratégiája?](#11-mi-a-lokális-keresések-általános-vezérlési-stratégiája)
    - [12. A tabu keresésnél használt kiértékelő függvény, amellyel össze tudjuk hasonlítani az aktuális csúcs gyerekeit, heurisztikus stratégiának számít?](#12-a-tabu-keresésnél-használt-kiértékelő-függvény-amellyel-össze-tudjuk-hasonlítani-az-aktuális-csúcs-gyerekeit-heurisztikus-stratégiának-számít)
  - [4](#4)
    - [1. Mely fogalmak kapcsolhatók egymáshoz a visszalépéses keresés esetén?](#1-mely-fogalmak-kapcsolhatók-egymáshoz-a-visszalépéses-keresés-esetén)
    - [2. Mit tartalmaz a visszalépéses keresések globális munkaterülete?](#2-mit-tartalmaz-a-visszalépéses-keresések-globális-munkaterülete)
    - [3. Melyek a visszalépéses keresés keresési szabályai?](#3-melyek-a-visszalépéses-keresés-keresési-szabályai)
    - [4. Mi a visszalépéses keresés általános vezérlési stratégiája?](#4-mi-a-visszalépéses-keresés-általános-vezérlési-stratégiája)
    - [5. Melyik állítás NEM igaz a visszalépéses keresés második változatára az alábbiak közül?](#5-melyik-állítás-nem-igaz-a-visszalépéses-keresés-második-változatára-az-alábbiak-közül)
    - [6. Melyek az alábbiak közül a visszalépéses keresés hátrányai? (2)](#6-melyek-az-alábbiak-közül-a-visszalépéses-keresés-hátrányai-2)
    - [7. Képzelje maga elé a 4-királynő probléma 2. állapottér modelljének állapotfáját. (Minden csúcsból négy él vezet ki.) Hány startcsúcsból kivezető utat vizsgál meg ebben a visszalépéses keresés második változata, ha a mélységi korlát 2?](#7-képzelje-maga-elé-a-4-királynő-probléma-2-állapottér-modelljének-állapotfáját-minden-csúcsból-négy-él-vezet-ki-hány-startcsúcsból-kivezető-utat-vizsgál-meg-ebben-a-visszalépéses-keresés-második-változata-ha-a-mélységi-korlát-2)
    - [8. Mely állítások igazak a visszalépéses keresés második változatára az alábbiak közül? (2)](#8-mely-állítások-igazak-a-visszalépéses-keresés-második-változatára-az-alábbiak-közül-2)
    - [9. Mely állítások NEM igazak a visszalépéses keresés második változatára az alábbiak közül? (2)](#9-mely-állítások-nem-igazak-a-visszalépéses-keresés-második-változatára-az-alábbiak-közül-2)
    - [10. Melyek az alábbiak közül a visszalépéses keresés előnyei? (3)](#10-melyek-az-alábbiak-közül-a-visszalépéses-keresés-előnyei-3)
    - [11. Mely állítások NEM igazak az alábbiak közül? (2)](#11-mely-állítások-nem-igazak-az-alábbiak-közül-2)
    - [12. Képzelje maga elé a Hanoi tornyai probléma állapotgráfját három korong esetén. A startcsúcsból kivezető utak közül hányat vizsgál meg a visszalépéses keresés második változata, ha a mélységi korlát 3?](#12-képzelje-maga-elé-a-hanoi-tornyai-probléma-állapotgráfját-három-korong-esetén-a-startcsúcsból-kivezető-utak-közül-hányat-vizsgál-meg-a-visszalépéses-keresés-második-változata-ha-a-mélységi-korlát-3)
  - [5](#5)
    - [1. Mit tartalmaz a gráfkeresés globális munkaterülete?](#1-mit-tartalmaz-a-gráfkeresés-globális-munkaterülete)
    - [2. Melyek a gráfkeresés keresési szabályai?](#2-melyek-a-gráfkeresés-keresési-szabályai)
    - [3. Mi a gráfkeresés általános vezérlési stratégiája?](#3-mi-a-gráfkeresés-általános-vezérlési-stratégiája)
    - [4. Mely csúcsokat nevezzük a gráfkereséseknél nyílt csúcsoknak?](#4-mely-csúcsokat-nevezzük-a-gráfkereséseknél-nyílt-csúcsoknak)
    - [5. Mit mutat a gráfkereséseknél a szülőre visszamutató pointerfüggvény (𝜋)?](#5-mit-mutat-a-gráfkereséseknél-a-szülőre-visszamutató-pointerfüggvény-𝜋)
    - [6. Mit mutat a gráfkereséseknél a költségfüggvény (g)?](#6-mit-mutat-a-gráfkereséseknél-a-költségfüggvény-g)
    - [7. Mikor nevezünk egy kiértékelő függvényt csökkenőnek?](#7-mikor-nevezünk-egy-kiértékelő-függvényt-csökkenőnek)
    - [8. Hogyan lehet a keresőgráf korrektségét fenn tartani? (2)](#8-hogyan-lehet-a-keresőgráf-korrektségét-fenn-tartani-2)
    - [9. Mikor mondjuk a keresőgráf egyik csúcsára, hogy korrekt? (2)](#9-mikor-mondjuk-a-keresőgráf-egyik-csúcsára-hogy-korrekt-2)
    - [10. Mely állítások igazak az alábbiak közül a gráfkeresés általános algoritmusára? (3)](#10-mely-állítások-igazak-az-alábbiak-közül-a-gráfkeresés-általános-algoritmusára-3)
    - [11. Mely állítások NEM igazak az alábbiak közül a gráfkeresés általános algoritmusára? (2)](#11-mely-állítások-nem-igazak-az-alábbiak-közül-a-gráfkeresés-általános-algoritmusára-2)
    - [12](#12)
  - [6 Kviz](#6-kviz)
    - [1. Lehet-e sorrendi heurisztika egy nem-informált gráfkeresés másodlagos vezérlési stratégiájában?](#1-lehet-e-sorrendi-heurisztika-egy-nem-informált-gráfkeresés-másodlagos-vezérlési-stratégiájában)
    - [2. Mit jelent a gráfkereséseknél a megengedhetőség fogalma?](#2-mit-jelent-a-gráfkereséseknél-a-megengedhetőség-fogalma)
    - [3. Melyik állítás NEM igaz az azonosan nulla függvényről?](#3-melyik-állítás-nem-igaz-az-azonosan-nulla-függvényről)
    - [4. Melyik gráfkereső algoritmust nevezzük A* algoritmusnak?](#4-melyik-gráfkereső-algoritmust-nevezzük-a-algoritmusnak)
    - [5. 𝛿-gráfban egy csúcsot legfeljebb egyszer terjeszt ki.](#5-𝛿-gráfban-egy-csúcsot-legfeljebb-egyszer-terjeszt-ki)
    - [6. Mely állítás NEM igaz a következetes (Ac) algoritmusra?](#6-mely-állítás-nem-igaz-a-következetes-ac-algoritmusra)
    - [7. Mennyi a B algoritmus kiterjesztéseinek száma legrosszabb esetben, ha a kiterjesztett csúcsok száma k?](#7-mennyi-a-b-algoritmus-kiterjesztéseinek-száma-legrosszabb-esetben-ha-a-kiterjesztett-csúcsok-száma-k)
    - [8. Mikor mondunk egy A* algoritmust jobban informáltnak egy másiknál?](#8-mikor-mondunk-egy-a-algoritmust-jobban-informáltnak-egy-másiknál)
    - [9. Mikor mondjuk a gráfkereséseknél egy heurisztikus függvényről azt, hogy monoton megszorításos?](#9-mikor-mondjuk-a-gráfkereséseknél-egy-heurisztikus-függvényről-azt-hogy-monoton-megszorításos)
    - [10. Melyik állítás igaz az egyenletes gráfkeresésre? (2)](#10-melyik-állítás-igaz-az-egyenletes-gráfkeresésre-2)
    - [11. Az alábbiak közül melyek a megengedhető gráfkereső algoritmusok?(4)](#11-az-alábbiak-közül-melyek-a-megengedhető-gráfkereső-algoritmusok4)
    - [12 Mely fogalmak kapcsolhatók egymáshoz a gráfkereséseknél?](#12-mely-fogalmak-kapcsolhatók-egymáshoz-a-gráfkereséseknél)
  - [7](#7)
    - [1. A kurzuson speciális kétszemélyes játékokkal foglalkoztunk. Az alábbiak közül melyik tulajdonság NEM volt érvényes ezekre?](#1-a-kurzuson-speciális-kétszemélyes-játékokkal-foglalkoztunk-az-alábbiak-közül-melyik-tulajdonság-nem-volt-érvényes-ezekre)
    - [2. Hogyan modellezzük a kétszemélyes játékokat?](#2-hogyan-modellezzük-a-kétszemélyes-játékokat)
    - [3. Mi a nyerő stratégiája egy játékosnak egy kétszemélyes játékban?](#3-mi-a-nyerő-stratégiája-egy-játékosnak-egy-kétszemélyes-játékban)
    - [4. Melyik állítás igaz az alábbiak közül egy játékos nyerő stratégiára?](#4-melyik-állítás-igaz-az-alábbiak-közül-egy-játékos-nyerő-stratégiára)
    - [5. Hogyan lehet megtudni, hogy kinek van győztes stratégiája egy két kimenetelű kétszemélyes játékban? (2)](#5-hogyan-lehet-megtudni-hogy-kinek-van-győztes-stratégiája-egy-két-kimenetelű-kétszemélyes-játékban-2)
    - [6. Mikor következik be vágás az alfa-béta algoritmus működése során?](#6-mikor-következik-be-vágás-az-alfa-béta-algoritmus-működése-során)
    - [7. Mi az a nyugalmi teszt? (2)](#7-mi-az-a-nyugalmi-teszt-2)
    - [8. Mely állítások igazak az alábbiak közül a játékfákra? (3)](#8-mely-állítások-igazak-az-alábbiak-közül-a-játékfákra-3)
    - [9. Melyek az alábbiak közül a minimax algoritmusnak a lépései?](#9-melyek-az-alábbiak-közül-a-minimax-algoritmusnak-a-lépései)
    - [10. Az alábbi részleges játékfa kiértékelő módszerek közül melyik ad a minimax-szal azonos eredményt? (2)](#10-az-alábbi-részleges-játékfa-kiértékelő-módszerek-közül-melyik-ad-a-minimax-szal-azonos-eredményt-2)
    - [11. Mi a játékfa?](#11-mi-a-játékfa)
    - [12. Mely fogalmak kapcsolhatók egymáshoz a részleges játékfa-kiértékeléseknél?](#12-mely-fogalmak-kapcsolhatók-egymáshoz-a-részleges-játékfa-kiértékeléseknél)
  - [8](#8)
    - [1. Milyen az általános vezérlési stratégiája az evolúviós algoritmusoknak?](#1-milyen-az-általános-vezérlési-stratégiája-az-evolúviós-algoritmusoknak)
    - [2. Mit tárol az evolúciós algoritmus a globális munkaterületén?](#2-mit-tárol-az-evolúciós-algoritmus-a-globális-munkaterületén)
    - [3. Melyik NEM evolúciós operátor az alábbiak közül?](#3-melyik-nem-evolúciós-operátor-az-alábbiak-közül)
    - [4. Hogyan szokták az egyedeket kódolni?](#4-hogyan-szokták-az-egyedeket-kódolni)
    - [5. Hol épülhet véletlenített módszer az evolúciós algoritmusba?](#5-hol-épülhet-véletlenített-módszer-az-evolúciós-algoritmusba)
    - [6. Hol van szerepe a kiválasztásnak az evolúciós algoritmusban? (3)](#6-hol-van-szerepe-a-kiválasztásnak-az-evolúciós-algoritmusban-3)
    - [7. Mi a lényege a jó kiválasztási módszernek az evolúciós algoritmusokban?](#7-mi-a-lényege-a-jó-kiválasztási-módszernek-az-evolúciós-algoritmusokban)
    - [8. Mi a kapcsolat a keresztezés és a rekombináció között?](#8-mi-a-kapcsolat-a-keresztezés-és-a-rekombináció-között)
    - [9. Melyek lehetnek a feltételei az evolúciós algoritmus leállásának? (2)](#9-melyek-lehetnek-a-feltételei-az-evolúciós-algoritmus-leállásának-2)
    - [10. Mely keresztezési módszerek őrzik meg  permutáció tulajdonságot? (2)](#10-mely-keresztezési-módszerek-őrzik-meg--permutáció-tulajdonságot-2)
    - [11. Az alábbiak közül, melyek alkalmas módszerek a permutáció tulajdonságot megőrző mutációra? (2)](#11-az-alábbiak-közül-melyek-alkalmas-módszerek-a-permutáció-tulajdonságot-megőrző-mutációra-2)
    - [12. Mely fogalmak kapcsolhatók egymáshoz az evolúciós algoritmusoknál?](#12-mely-fogalmak-kapcsolhatók-egymáshoz-az-evolúciós-algoritmusoknál)
  - [9](#9)
    - [1. Mi az a rezolúciós gráf?](#1-mi-az-a-rezolúciós-gráf)
    - [2. Melyek a p || q   és a   !p || !q  rezolvensei?](#2-melyek-a-p--q---és-a---p--q--rezolvensei)
    - [3. Mi a globális munkaterülete a rezolúciónak?](#3-mi-a-globális-munkaterülete-a-rezolúciónak)
    - [4. Mi a keresési szabálya a rezolúciónak?](#4-mi-a-keresési-szabálya-a-rezolúciónak)
    - [5. Melyik az alábbiak közül a visszafelé haladó szabályalapú reprezentáció jellemzője?](#5-melyik-az-alábbiak-közül-a-visszafelé-haladó-szabályalapú-reprezentáció-jellemzője)
    - [6. Melyik az alábbiak közül az előrefelé haladó szabályalapú reprezentáció jellemzője?](#6-melyik-az-alábbiak-közül-az-előrefelé-haladó-szabályalapú-reprezentáció-jellemzője)
    - [7. Hogyan kell a rezolúciót válaszadásra felhasználni?](#7-hogyan-kell-a-rezolúciót-válaszadásra-felhasználni)
    - [8. Mi következik abból, hogy a rezolúció módszere helyes? (2)](#8-mi-következik-abból-hogy-a-rezolúció-módszere-helyes-2)
    - [9. Mi következik abból, hogy a rezolúció módszere teljes? (2)](#9-mi-következik-abból-hogy-a-rezolúció-módszere-teljes-2)
    - [10. Melyek az alábbiak közül a rezolúció reprezentációs gráfjának különös tulajdonságai? (2)](#10-melyek-az-alábbiak-közül-a-rezolúció-reprezentációs-gráfjának-különös-tulajdonságai-2)
    - [11. Melyek lehetnek az alábbiak közül a rezolúció modellfüggő vágó stratégiái? (2)](#11-melyek-lehetnek-az-alábbiak-közül-a-rezolúció-modellfüggő-vágó-stratégiái-2)
    - [12. Melyek az alábbiak közül a rezolúció modellfüggő sorrendi stratégiái? (2)](#12-melyek-az-alábbiak-közül-a-rezolúció-modellfüggő-sorrendi-stratégiái-2)
  - [10](#10)
    - [1. Hogyan számoljuk az A esemény valószínűségét feltéve, hogy B esemény – amely valószínűsége nagyobb, mint nulla – bekövetkezik?](#1-hogyan-számoljuk-az-a-esemény-valószínűségét-feltéve-hogy-b-esemény--amely-valószínűsége-nagyobb-mint-nulla--bekövetkezik)
    - [2. Mikor mondjuk, hogy A és B események feltételesen függetlenek E eseményre nézve?](#2-mikor-mondjuk-hogy-a-és-b-események-feltételesen-függetlenek-e-eseményre-nézve)
    - [3. Az alábbiak közül melyik egy Bayes tétel?](#3-az-alábbiak-közül-melyik-egy-bayes-tétel)
    - [4. Az alábbiak közül melyik NEM igényel bizonytalanság kezelést?](#4-az-alábbiak-közül-melyik-nem-igényel-bizonytalanság-kezelést)
    - [5. Milyen gráf a valószínűségi háló?](#5-milyen-gráf-a-valószínűségi-háló)
    - [6. Mit mutat meg a valószínűségi háló feltételes valószínűségi táblája?](#6-mit-mutat-meg-a-valószínűségi-háló-feltételes-valószínűségi-táblája)
    - [7. Mit jelent a normalizálás technikája? (2)](#7-mit-jelent-a-normalizálás-technikája-2)
    - [8. Mit jelent az, hogy egy valószínűsági háló egyszeresen kötött?](#8-mit-jelent-az-hogy-egy-valószínűsági-háló-egyszeresen-kötött)
    - [9. Az alábbiak közül melyek igazak a valószínűségi hálókra? (2)](#9-az-alábbiak-közül-melyek-igazak-a-valószínűségi-hálókra-2)
    - [10. Hogyan javítható a valószínűségi hálóban való számítás hatékonysága, ha a háló nem fa-gráf? (3)](#10-hogyan-javítható-a-valószínűségi-hálóban-való-számítás-hatékonysága-ha-a-háló-nem-fa-gráf-3)
    - [11. Milyen heurisztikus bizonytalanságkezelő technikákról hallott? (2)](#11-milyen-heurisztikus-bizonytalanságkezelő-technikákról-hallott-2)
    - [12. Mely fogalmak kapcsolhatók egymáshoz a bizonytalanság kezelésnél?](#12-mely-fogalmak-kapcsolhatók-egymáshoz-a-bizonytalanság-kezelésnél)
  - [11](#11)
    - [1. Mit jelent az, hogy egy tanulás felügyelt?](#1-mit-jelent-az-hogy-egy-tanulás-felügyelt)
    - [2. Mit jelent az, hogy egy tanulás felügyelet nélküli?](#2-mit-jelent-az-hogy-egy-tanulás-felügyelet-nélküli)
    - [3. Mit jelent a zaj a tanító minták esetén?](#3-mit-jelent-a-zaj-a-tanító-minták-esetén)
    - [4. Különböző tanító minták halmazának mikor a legkisebb az információ (entrópia) tartalma a döntési fáknál?](#4-különböző-tanító-minták-halmazának-mikor-a-legkisebb-az-információ-entrópia-tartalma-a-döntési-fáknál)
    - [5. Hogyan értékelünk ki a döntési fa építése során egy levélcsúcsot akkor, ha nem tartoznak hozzá tanító minták?](#5-hogyan-értékelünk-ki-a-döntési-fa-építése-során-egy-levélcsúcsot-akkor-ha-nem-tartoznak-hozzá-tanító-minták)
    - [6. A döntési fa építése során az alábbiak közül milyen csúcsok fordulhatnak elő a fában? (3)](#6-a-döntési-fa-építése-során-az-alábbiak-közül-milyen-csúcsok-fordulhatnak-elő-a-fában-3)
    - [7. Mely állítások igazak a döntési fára? (2)](#7-mely-állítások-igazak-a-döntési-fára-2)
    - [8. Mely állítások igazak a döntési fa módszerére? (2)](#8-mely-állítások-igazak-a-döntési-fa-módszerére-2)
    - [9 Mely állítások igazak k-legközelebbi szomszéd módszerére? (2)](#9-mely-állítások-igazak-k-legközelebbi-szomszéd-módszerére-2)
    - [10. Milyen felügyelt tanulási módszereket ismert meg a kurzuson? (3)](#10-milyen-felügyelt-tanulási-módszereket-ismert-meg-a-kurzuson-3)
    - [11. Hol jutott szerepe a véletlennek a véletlen erdő módszerében? (2)](#11-hol-jutott-szerepe-a-véletlennek-a-véletlen-erdő-módszerében-2)
    - [12. Mely fogalmak kapcsolhatók össze az alábbiak közül a gépi tanulás témakörében?](#12-mely-fogalmak-kapcsolhatók-össze-az-alábbiak-közül-a-gépi-tanulás-témakörében)

## 2

### 1. Hogyan NEM csökkenthető egy állapottér modell bonyolultsága? 

* **Y** *Csökkentjük a célállapotok számát*
* X Szigorítjuk az állapotok invariáns tulajdonságát.
* X Szigorítjuk a műveletek értelmezési tartományát.
* X Növeljük az állapotok számát, de új műveleteket vezetünk be.

### 2. Mitől NEM függ egy reprezentációs gráf bonyolultsága? 

* X A köreinek gyakoriságától, és hosszuk sokféleségétől
* X A csúcsai ki-fokának számától.
* **Y** *A csúcsai be-fokának számától.*
* X A csúcsainak és éleinek számátó
 
### 3. Melyik NEM része a probléma dekompozíciós modellnek?

* X A kiinduló probléma leírása.
* X Dekompozíciós műveletek definiálása.
* X Az egyszerű problémák megadása.
* **Y** *Az állapotok definiálása.*

### 4. Milyen egy dekompozíciós operátor?

* X Egy problémát több problémának a halmazára képez le.
* X Egy probléma-sorozatot részsorozatokra bont fel.
* X Egy problémát megadott problémák egyikével helyettesít
* **Y** *Egy problémát több problémának a sorozatára képez le.*

### 5. Az alábbiak közül melyek NEM elemei az állapottér modellnek? (2) 

* **Y** *heurisztika*
* X kezdő állapot vagy annak leírása
* X műveletek
* **Y** *állapotgráf*

### 6. Mely állítások igazak az állapotgráfra az alábbiak közül? (3)

* **Y** *Startcsúcsa a kezdőállapotot szimbolizálja.*
* **Y** *Élei a műveletek végrehajtásait szimbolizálják.*
* **Y** *Csúcsai az állapotokat szimbolizálják.*
* X Célcsúcsai a modellezett feladat megoldásai.

### 7. Az alábbi feladat-modellezések közül melyeknél NEM egyezett meg a problématér a reprezentációs gráf startcsúcsból kivezető útjaival? (2)

* **Y** *n-királynő probléma*
* X Hanoi-tornyai probléma
* **Y** *integrál számítás*
* X 8-as kirakó játék

### 8. Melyik ok-okozati összefüggések igazak az alábbiak közül? (2)

* **Y** *Az állapotgráf csúcsainak száma kihat a megoldó algoritmus hatékonyságára.*
* X Az optimális megoldások száma kihat az állapotgráf bonyolultságára.
* X A megoldó algoritmus számítási bonyolultsága kihat a problématér bonyolultságára.
* **Y** *Az állapotgráfbeli körök hossza és száma kihat a problématér bonyolultságára.*

### 9. Hogyan csökkenthető egy állapottér modellben a műveletek kiszámítási bonyolultsága? (2)

* X Szigorítjuk a műveletek előfeltételét.
* **Y** *Szigorítjuk az állapotok invariáns állítását.*
* X Több heurisztikát építünk be a modellbe.
* **Y** *Az állapotokat extra információval egészítjük ki.*

### 10. Mely fogalmak kapcsolhatók egymáshoz?

* dekompozíciós operátor - hiperél
* állapot - csúcs
* művelet - irányított él
* dekompozíciós folyamat - hiperút

### 11. Melyek a feltételei a visszafelé haladó keresésnek?

* X A reprezentációs gráf startcsúcsából valamelyik célcsúcsba vezető úton kétirányú élek legyenek.
* **Y** *A reprezentációs gráf kétirányú éleket tartalmazzon és legyen ismert valamelyik célállapot.*
* X A reprezentációs gráf startcsúcsából az összes célcsúcsba vezető úton kétirányú élek legyenek.
* X A reprezentációs gráf kétirányú éleket tartalmazzon és legyen ismert az összes célállapot.

### 12. Mi célt szolgál a probléma-redukciós operátor?

* X Egy problémát egyszerűbb problémákra vezet vissza.
* X MEgadja, hogy egy állapot mely állapotokból érhető el egy állapottér modellben.
* X Egy állapottér modell egy műveletének inverzze.
* **Y** *Az állapottér modell egy műveletére megadja, hogy a művelet segítségével mely állapotokból lehet eljutni adott állapotok egyikébe.*


## 3

### 1. Az alábbi módszerek közül melyiknél változhat futás közben a globális munkaterület mérete?

* X Hegymászó módszernél. 
* X Véletlen újra indított hegymászó módszernél.     
* X Szimulált hűtésnél.  
* **Y** *Tabu keresésnél.*


### 2. Melyik állítás NEM igaz a lokális keresésekre az alábbiak közül? (2)

* X Memóriája az aktuális csúcs környezetének tárolására korlátozódik.  
* X Az aktuális csúcs környezetéből választja az új aktuális csúcsot.      
* **Y** *Csak egy lokálisan legjobb megoldást képes megtalálni.*
* **Y** *Ezek mohó stratégiájú algoritmusok.*

### 3. Tekinthető-e a hegymászó módszer a tabu keresés speciális változatának? (2)

* **Y** *Igen, amennyiben a hegymászó módszer tulajdonképpen egy egyelemű tabu halmazt használ, amely az előző aktuális csúcsot tárolja csak.*
* **Y** *Nem, amennyiben a hegymászó módszer nem tárolja el az eddig megtalált legjobb kiértékelő függvényértékű csúcsot.*
* X Nem, mert a tabu keresés véletlen módon választ új csúcsot.  
* X Nem, mert a tabu keresés felismeri a köröket, a hegymászó algoritmus nem.


### 4. Hány helyen használ a szimulált hűtés algoritmusa véletlenített módszert?

* X Három. A következő aktuális csúcs kiválasztásához, annak elfogadásához, és a hűtési ütemterv változtatásához.  
* X Egy. A következő aktuális csúcs kiválasztásához.      
* **Y** *Kettő. A következő csúcs kiválasztásához, illetve annak elfogadásához.*
*  X Nulla. Ez ugyan egy nem-determinisztikus módszer, de nem használ véletlenítést.


### 5. Mely állítások igazak az alábbiak közül? (2)

* X A heurisztika garantálja, hogy az algoritmus az optimális megoldást találja meg.  
* X A heurisztika garantálja, hogy az algoritmus hatékonysága jobb lesz.     
* **Y** *A heurisztikát a feladatot megoldó algoritmusba közvetlenül építjük be.*
* **Y** *A heurisztika egyszerre csökkentheti az algoritmus memória igényét és a futási idejét.*


### 6. Melyek az alábbiak közül a tabu keresés hátrányai? (2)

* **Y** *A tabu halmaz méretét csak kísérletezéssel lehet beállítani.*
* X Képes felismerni, és elkerülni a kisebb köröket.      
* X Kicsi a memória igénye  
* **Y** *Zsákutcába érve a keresés megáll.*


### 7. Mely állítások NEM igazak a lokális keresésre az alábbiak közül? (2)

* **Y** *Talál megoldást, ha van megoldás*
* X Erősen összefüggő gráfokban nem akadnak el.     
* X Kicsi memóriát használnak.  
* **Y** *Körmentes gráfokban nem akad el.*


### 8. Melyek az alábbiak közül a hegymászó módszer hátrányai? (3)

* **Y** *Körök mentén végtelen működésbe kezdhet.*
* X Kicsi a memória igénye.       
* **Y** *Zsákutcába érve a keresés megáll.*
* **Y** *Nem garantál optimális megoldást*


### 9. Hogyan hat a heurisztika információ tartalma egy kereső rendszer futási idejére? (3)

* **Y** *Minél kisebb az információ tartalma, annál gyorsabban tud új lépést választani.*
* **Y** *Nagyobb információ tartalom mellett egy lépés futási ideje nő.*
* **Y** *Nagyobb információ tartalom mellett a lépések száma csökkenhet.*
* X Minél nagyobb az információ tartalma, annál jobb lesz a hatékonysága. 


### 10. Mely algoritmusok születtek a hegymászó módszer zsákutcában való beragadásának elkerülésére? (3)

* **Y** *Lokális nyaláb keresés (local beam search)*
* **Y** *Szimulált hűtés algoritmusa*
* X Tabu keresés  
* **Y** *Véletlen újraindított keresés (random restart search)*


### 11. Mi a lokális keresések általános vezérlési stratégiája?

* X Az aktuális csúcs(ok) környezetéből válasszuk a legjobb csúcsot (csúcsokat)!
* X Az aktuális csúcs szomszédjai közül válasszuk a legjobb csúcsot!    
* X Az aktuális csúcs környezetéből válasszuk a legjobb csúcsot!  
* **Y** *Az aktuális csúcs(ok) környezetéből válasszunk egy (vagy több) viszonylag jó csúcsot!*

### 12. A tabu keresésnél használt kiértékelő függvény, amellyel össze tudjuk hasonlítani az aktuális csúcs gyerekeit, heurisztikus stratégiának számít?

* X Nem, mert ilyen függvényt minden tabu keresés használ 
* X Nem, mert ezt csak az olyan feladatoknál használhatjuk, amelyek állapottér modell-lel rendelkeznek. Ez tehát egy modell-függő stratégia.    
* X A heurisztikának nincs köze a vezérlési stratégiához.
* **Y** *Igen, ez a függvény a konkrét feladatból származik.*

## 4

### 1. Mely fogalmak kapcsolhatók egymáshoz a visszalépéses keresés esetén?

* globális munkaterület - irányított út
* keresési szabály - visszalépés
* másodlagos vezérlési stratégia - sorrendi szabály  
* második változat - mélységi korlátfigyeés

### 2. Mit tartalmaz a visszalépéses keresések globális munkaterülete?

* X Ez  eddig bejárt részgráfot és külön annak a startcsúcsból kiinduló egyik útját annak csúcsaiból kivezető még nem vizsgált élekkel együtt.
* **Y** *A startcsúcsból kiinduló egyik utat és annak csúcsaiból kivezető még nem vizsgált éleket.*
* X A reprezentációs gráfot és külön annak a startcsúcsból kiinduló egyik útját.
* X Ez eddig bejárt startcsúcsból kiinduló utakat azok csúcsaiból kivezető még nem vizsgált élekkel együtt. 


### 3. Melyek a visszalépéses keresés keresési szabályai?

* X A nyilvántartott út utolsó csúcsának kiterjesztése, illetve az utolsó él elvétele.
* X A nyilvántartott út kiterjesztése, illetve a visszalépés.
* **Y** *A nyilvántartott út végcsúcsából kivezető egyik él hozzávétele az úthoz, illetve az út utolsó élének elvétele.*
* X A nyilvántartott úthoz egy újabb kivezető él hozzávétele, illetve az utolsó él elvétele.

### 4. Mi a visszalépéses keresés általános vezérlési stratégiája?

* X A továbblépést meghatározó sorrendi és a vágó szabályok.
* X Zsákutcába jutva mindig a visszalépés szabályát kell választani.
* X A visszalépés szabálya mindig elsőbbséget élvez a többi keresési szabállyal szemben. 
* **Y** *A visszalépés szabályát csak a legvégső esetben válasszuk.*

### 5. Melyik állítás NEM igaz a visszalépéses keresés második változatára az alábbiak közül?

* X A körfigyelés elhagyása végtelen fák esetén mindenképpen gyorsítja a megoldás megtalálását. 
* X A körfigyelés elhagyása növeli a memória igényét. 
* X A körfigyelés elhagyása kicsi mélységi korlát mellett gyorsíthatja a futási időt.
* **Y** *A körfigyelés elhagyása mindenképpen gyorsítja a megoldás megtalálását.*

### 6. Melyek az alábbiak közül a visszalépéses keresés hátrányai? (2)

* X Nehéz az implementációja. 
* X Nagy a memória igénye. 
* **Y** *Kezdetben hozott rossz döntést csak sok visszalépés árán korrigálja.*
* **Y** *Ugyanazt a részgráfot többször is bejárja.*

### 7. Képzelje maga elé a 4-királynő probléma 2. állapottér modelljének állapotfáját. (Minden csúcsból négy él vezet ki.) Hány startcsúcsból kivezető utat vizsgál meg ebben a visszalépéses keresés második változata, ha a mélységi korlát 2?

* X 8
* X 20
* X 16
* **Y** *21*

### 8. Mely állítások igazak a visszalépéses keresés második változatára az alábbiak közül? (2)


* X Minden 𝛿-gráfban talál megoldást, ha van. 
* **Y** *Minden 𝛿-gráfban terminál.*
* X Minden 𝛿-gráfban megmutatja, hogy van-e megoldás.
* **Y** *Minden 𝛿-gráfban talál megoldást, ha annak hossza rövidebb, mint a mélységi korlát.*

### 9. Mely állítások NEM igazak a visszalépéses keresés második változatára az alábbiak közül? (2)

* X A mélységi korlát figyelés önmagában is elég ahhoz, hogy garantáltan termináljon.
* **Y** *Képes megtalálni a legrövidebb megoldást, ha van.*
* X Ha van megoldás a mélységi korláton belül, akkor talál megoldást. 
* **Y** *A körfigyelés önmagában is elég ahhoz, hogy garantáltan termináljon.*

### 10. Melyek az alábbiak közül a visszalépéses keresés előnyei? (3)

* **Y** *Ha van (mélységi korálton belül) megoldása, akkor talál egyet.*
* **Y** *Mindig terminál.*
* **Y** *Kicsi a memória igénye.*
* X Véges 𝛿-gráfban optimális megoldást talál. 


### 11. Mely állítások NEM igazak az alábbiak közül? (2)

* **Y** *A sorrendi szabály egy heurisztikus vezérlési stratégia.*
* X A sorrendi és a vágó szabály egyaránt épülhet heurisztikára. 
* X A mélységi korlát felfogható egy speciális vágó szabálynak. 
* **Y** *Vágó szabály nem alkalmazható sorrendi szabályokkal együtt.*

 
### 12. Képzelje maga elé a Hanoi tornyai probléma állapotgráfját három korong esetén. A startcsúcsból kivezető utak közül hányat vizsgál meg a visszalépéses keresés második változata, ha a mélységi korlát 3?

* X 14
* X 9
* X 8
* **Y** *15*

## 5

### 1. Mit tartalmaz a gráfkeresés globális munkaterülete?

* **Y** *A startcsúcsból kiinduló eddig felfedezett összes utat a nyílt csúcsokkal együtt.*
* A reprezentációs gráfot, de külön megcímkézve benne a már bejárt csúcsokat.
* A reprezentációs gráf egy tetszőleges részgráfját. 
* Csak a nyílt csúcsok halmazát. 

### 2. Melyek a gráfkeresés keresési szabályai?
* **Y** *A nyílt csúcsok kiterjesztései.* 
* Egy újabb él hozzávétele a kereső gráf egyik csúcsához. 
* A továbblépés (újabb él felfedezése) és a visszalépés. 
* A továbblépés (egy csúcsból kivezető összes él felfedezése) és a visszalépés.

### 3. Mi a gráfkeresés általános vezérlési stratégiája?

* **Y** *Minden lépésben a legígéretesebb nyílt csúcsot választja kiterjesztésre.*
* A legutoljára felfedezett nyílt csúcs kiterjesztése. 
* A startcsúcsból legkisebb költségű úton elérhető nyílt csúcs kiterjesztése.
* A startcsúcsból legkisebb költségű már felfedezett úton elérhető nyílt csúcs kiterjesztése.


### 4. Mely csúcsokat nevezzük a gráfkereséseknél nyílt csúcsoknak?
* **Y** *A keresőgráf azon csúcsait, amelyek gyermekeit még nem, vagy nem eléggé jól ismerjük, ennél fogva kiterjesztésre várnak.*
* A keresőgráf azon csúcsait, amelyekből kivezető éleket még nem fedeztük fel. 
* A keresőgráf azon csúcsait, amelyeket még nem terjesztettünk ki.
* A reprezentációs gráf azon csúcsait, amelyeket még nem terjesztettünk ki. 



### 5. Mit mutat a gráfkereséseknél a szülőre visszamutató pointerfüggvény (𝜋)?

* **Y** *A keresőgráfbeli csúcsok egyik szülőjét.*
* A reprezentációs gráfbeli csúcsok legjobb szülőjét. 
* A keresőgráfbeli csúcsok legjobb szülőjét. 
* A reprezentációs gráfbeli csúcsok egyik szülőjét. 


### 6. Mit mutat a gráfkereséseknél a költségfüggvény (g)?
* **Y** *A startcsúcsból a keresőgráfbeli csúcsokhoz, a keresőgráfban vezető egyik út költségét.*
* A startcsúcsból a keresőgráfbeli csúcsokhoz vezető egyik út költségét. 
* A startcsúcsból a keresőgráfbeli csúcsokhoz, a keresőgráfban vezető legolcsóbb út költségét.
* A startcsúcsból a keresőgráfbeli csúcsokhoz a szülőre visszamutató pointerfüggvény által kijelölt út költségét.


### 7. Mikor nevezünk egy kiértékelő függvényt csökkenőnek?

* **Y** *Ha egy csúcs függvényértéke soha nem nő, viszont mindig csökken valahányszor olcsóbb odavezető utat találunk hozzá.*
* Ha egy csúcs értéke csak akkor változik, de akkor csökken, ha egy olcsóbb odavezető utat találunk hozzá.
* Ha egy startcsúcsból kiinduló már felfedezett út mentén a csúcsok függvényértékei monoton csökkennek.
* Ha az algoritmus által kiterjesztett csúcsok függvényértékei monoton csökkennek.


### 8. Hogyan lehet a keresőgráf korrektségét fenn tartani? (2)
* **Y** *Minden kiterjesztés után bejárjuk a kiterjesztéssel elért gyerekcsúcsok leszármazottait (ha vannak), és kijavítjuk azok korrektségét.*
* **Y** *Olyan kiértékelő függvényt használunk, amely kizárja, hogy egy már korábban kiterjesztett csúcshoz minden addiginál olcsóbb odavezető utat találjunk a startcsúcsból.* 
* Visszahelyezzük az OPEN halmazba azt a zárt csúcsot, amelyhez minden addiginél olcsóbb odavezető utat találtunk a startcsúcsból.
* Amikor egy minden addiginél olcsóbb odavezető utat találunk egy csúcshoz, akkor módosítjuk a szülőre visszamutató pointerfüggvény értékét és a költségfüggvény értékét. 

### 9. Mikor mondjuk a keresőgráf egyik csúcsára, hogy korrekt? (2)

* **Y** *Ha a szülőre visszamutató pointerek a keresőgráfra nézve optimális utat jelölnek ki hozzá a startcsúcsból, és ennek az útnak a költségét mutatja a költségfüggvény.*
* **Y** *Ha optimális és konzisztens.*
* Ha a gráfkeresés már kiterjesztette a gyerekeit is.
* Ha a költségfüggvény értéke a visszamutató pointerfüggvény által kijelölt szülő csúcsánál mért költségfüggvény értékének, és a szülőtől hozzávezető él költségének összege. 


### 10. Mely állítások igazak az alábbiak közül a gráfkeresés általános algoritmusára? (3)
* **Y** *Véges 𝛿-gráfban mindig terminál.*
* **Y** *Egy csúcsot legfeljebb véges sokszor terjeszt ki még végtelen nagy 𝛿-gráfok esetén is.*
* **Y** *Véges 𝛿-gráfban talál megoldást, ha van.*
* Véges 𝛿-gráfban optimális megoldást talál, ha van megoldás. 

### 11. Mely állítások NEM igazak az alábbiak közül a gráfkeresés általános algoritmusára? (2)

* **Y** *Körmentes 𝛿-gráfban talál megoldást, ha van.*
* **Y** *𝛿-gráfban mindig terminál.*
* Csökkenő kiértékelő függvényt használva soha nem terjeszt ki inkorrekt csúcsot.
* Véges 𝛿-gráfban talál megoldást, ha van. 


### 12 
* globális munkaterület - keresőgráf
* keresési szabály - kiterjesztés
* pointerfüggvény - szülőcsúcs
* csökkenő kiértékelő függvény - korrektség

## 6 Kviz


### 1. Lehet-e sorrendi heurisztika egy nem-informált gráfkeresés másodlagos vezérlési stratégiájában?
* **Y** *Igen*
* Nem
* Csak akkor, ha már az elsődleges vezérlési stratégia is alkalmaz heurisztikát. 
* A másodlagos stratégiába nem lehet heurisztikát beépíteni. 

### 2. Mit jelent a gráfkereséseknél a megengedhetőség fogalma?
* **Y** *Olyan heurisztikus függvényt, amely alulról becsüli egy reprezentációs gráfban a csúcsokból a célba vezető optimális út költségét.*
* Olyan gráfkereső algoritmust, amelyik optimális megoldást talál, ha van. 
* Olyan algoritmust, amely lépésről lépésre szűkíti a megoldások halmazát, amíg az már csak az optimális megoldásokat tartalmazza.
* Olyan gráfkereséseket, amelyek kiértékelő függvényében megengedett a heurisztika használata. 

### 3. Melyik állítás NEM igaz az azonosan nulla függvényről?
* Nem tartalmaz extra ismeretet, azaz heurisztikát. 
* Megengedhető és monoton megszorításos. 
* Becsli a célba vezető optimális út költségét. 
* **Y** *Nem válaszható kiértékelő függvénynek.* 


### 4. Melyik gráfkereső algoritmust nevezzük A* algoritmusnak?

* **Y** *Amelyik kiértékelő függvénye g+h alakú, ahol h nem-negatív és megengedhető.*
* Amelyik garantáltan optimális megoldást talál, ha van. 
* Amelyik kiértékelő függvénye g+h alakú, ahol h nem-negatív, megengedhető és monoton megszorításos. 
* Amelyik kiértékelő függvénye g+h alakú, ahol h megengedhető, és garantáltan optimális megoldást talál, ha van. 

### 5. 𝛿-gráfban egy csúcsot legfeljebb egyszer terjeszt ki. 
* 𝛿-gráfban egy csúcsot legfeljebb egyszer terjeszt ki. 
* Heurisztikus függvénye megengedhető. 
* 𝛿-gráfban optimális megoldást talál, ha van.
* **Y** *𝛿-gráfban megengedhető heurisztikával optimális megoldást talál, ha van.* 


### 6. Mely állítás NEM igaz a következetes (Ac) algoritmusra?
*  Egy csúcsot legfeljebb egyszer terjeszt ki. 
* **Y** *A kiterjesztéseinek száma akár a kiterjesztett csúcsok száma mínusz egynek a kettő hatványa is lehet.*
* Amikor egy csúcsot kiterjeszt, már ismeri a start csúcsból odavezető optimális utat.
* 𝛿-gráfban optimális megoldást talál, ha van.


### 7. Mennyi a B algoritmus kiterjesztéseinek száma legrosszabb esetben, ha a kiterjesztett csúcsok száma k?
* k
* k log2 k
* 2^k-1
* **Y** *1/2 * k^2*

### 8. Mikor mondunk egy A* algoritmust jobban informáltnak egy másiknál?
* Ha kevesebb csúcs kiterjesztése mellett terminál. 
* Ha a memória igénye nem nagyobb a másikénál.
* Ha a heurisztikus függvényének értéke a nem célcsúcsokban közelebbi becslést ad, mint a másik algoritmus heurisztikus függvényének értéke.
* **Y** *Ha a heurisztikus függvényének értéke a nem célcsúcsokban nagyobb, mint a másik algoritmus heurisztikus függvényének értéke.*

### 9. Mikor mondjuk a gráfkereséseknél egy heurisztikus függvényről azt, hogy monoton megszorításos?
* Ha a függvény alulról becsüli minden csúcsban a hátralevő optimális költséget. 
* Ha a függvény megengedhető és nem negatív.
* Ha a függvényt használó gráfkeresés működési grafikonja monoton növekedő.
* **Y** *Ha bármelyik él költsége nagyobb-egyenlő, mint az a különség, amit úgy kapunk, hogy az él kezdőcsúcsának függvényértékéből levonjuk a végcsúcsának függvényértékét.*

### 10. Melyik állítás igaz az egyenletes gráfkeresésre? (2)

* **Y** *Optimális megoldást talál, ha van.* 
* Dijkstra legrövidebb utak algoritmusának szinonimája.
* **Y** *Egy már kiterjesztett csúcshoz soha nem talál minden addiginál olcsóbb utat.*
* Kiértékelő függvénye az élek élköltségeit egységnyinek tekinti.

### 11. Az alábbiak közül melyek a megengedhető gráfkereső algoritmusok?(4)
* **Y** *A algoritmus*
* **Y** *B algoritmus*
* **Y** *Egyenletes gráfkeresés*
* **Y** *A algoritmus*

### 12 Mely fogalmak kapcsolhatók egymáshoz a gráfkereséseknél?
* mélységi gráfkeresés - Nem informált
* A* algoritmus - optimális
* B algoritmus - Martelli
* memória igény - zárt csúcsok száma


## 7

### 1. A kurzuson speciális kétszemélyes játékokkal foglalkoztunk. Az alábbiak közül melyik tulajdonság NEM volt érvényes ezekre?
* **Y egyik játékosnak biztos van győztes stratégiája**
* zéró összegű 
* véges 
* determinisztikus 

### 2. Hogyan modellezzük a kétszemélyes játékokat?
* **Y Állapottér modellel.**
* ÉS/VAGY fákkal. 
* Probléma dekompozícióval. 
* Korlátkielégítéses modellel. 

### 3. Mi a nyerő stratégiája egy játékosnak egy kétszemélyes játékban?
* A győztes végállásba vezető egyik játszmája.
* Győztes végállásba vezető játszmáinak összessége. 
* **Y Azon győztes végállásba vezető játszmáinak összessége, amelyek közül valamelyiket biztosan végig tudja játszani, ha nem hibázik.**
* Győztes végállásainak összessége. 

### 4. Melyik állítás igaz az alábbiak közül egy játékos nyerő stratégiára?
* **Y A játékfából a játékos szempontjából készített ÉS/VAGY fában egy olyan hiperút, amelyik a startcsúcsból csupa, a játékos számára nyerő végállásba vezet.**
* A játékfából készített ÉS/VAGY fában egy olyan hiperút, amelyik a startcsúcsból csupa, a játékos számára nyerő végállásba vezet. 
* Mindkét játékos számára előállítható. 
*  Az egyik játékos biztosan rendelkezik vele. 

### 5. Hogyan lehet megtudni, hogy kinek van győztes stratégiája egy két kimenetelű kétszemélyes játékban? (2)
* **Y Úgy, hogy a minimax algoritmust alkalmazzuk a teljes játékfára úgy, hogy az első játékos győztes állásaihoz +1-et, a vesztes állásaihoz -1-et rendelünk. Ha a gyökérbe felfuttatott érték +1, akkor az első játékosnak van győztes stratégiája, egyébként a másodiknak.**
* **Y A játékfa leveleit megcímkézzük annak a játékosnak a nevével, aki a levélcsúccsal jelzett állásban nyerni fog. Szintről szintre felfelé haladva az Y játékos szintjén levő csúcs, ha van Y címkéjű gyereke, akkor Y címkét kap; különben a másik játékos nevét írjuk oda. A gyökér címkéje adja meg a választ.**
* Átalakítjuk a játékfát ÉS/VAGY fává, és ebben keresünk olyan gyökérből induló hiperutat, amely vagy kizárólag az egyik, vagy kizárólag a másik játékos csupa győztes levélcsúcsába vezet.
* Nem lehet véges lépésben megválaszolni ezt a kérdést. 

### 6. Mikor következik be vágás az alfa-béta algoritmus működése során?
* Ha az aktuális csúcs alfa értéke nagyobb vagy egyenlő az alatta vagy felette levő csúcs béta értékénél.
* Ha az aktuális út egy alfa értéke kisebb vagy egyenlő az út egy béta értékénél.
* Ha az aktuális csúcs alfa értéke nagyobb vagy egyenlő a csúcs béta értékénél. 
* **Y Ha az aktuális út egy alfa értéke nagyobb vagy egyenlő az út egy béta értékénél.**

### 7. Mi az a nyugalmi teszt? (2)
* Az alfa-béta algoritmus vágási feltételét ellenőrző teszt.
* A heurisztikus kiértékelő függvény konstruálásához használt lehetséges módszer.
* **Y Egy szülőcsúcs és egy gyerekének kiértékelő függvényértékei különbségét vizsgáló teszt.**
* **Y Váltakozó mélységű keresésnél a részfa felépítéséhez használt feltétel.**

### 8. Mely állítások igazak az alábbiak közül a játékfákra? (3)
* **Y Szintjei a soron következő játékost szimbolizálják.**
* **Y Ágai a lehetséges játszmákat szimbolizálják.**
* **Y Csúcsai a játék állásait szimbolizálják.**
* Levelei a győztes állásokat szimbolizálják. 

### 9. Melyek az alábbiak közül a minimax algoritmusnak a lépései?
* Felépítjük a játékfát.
* **Y A saját szintjeink csúcsaihoz a gyerekeik értékeinek maximumát írjuk**
* Megadjuk a legnagyobb értékű levélcsúcshoz vezető ágat.
* **Y Kiértékeljük a felépített fa leveleit.**

### 10. Az alábbi részleges játékfa kiértékelő módszerek közül melyik ad a minimax-szal azonos eredményt? (2)
* **Y negamax algoritmus**
* (n,m) átlagoló algoritmus 
* szelektív algoritmus 
* **Y alfa-béta algoritmus**

### 11. Mi a játékfa?
* Olyan ÉS/VAGY fa, amelyik szintjeiről váltakozva vagy csak ÉS kapcsolatú élek indulnak ki, vagy csak VAGY kapcsolatú élek.
* **Y Az összes játszmát irányított útként megjelenítő irányított fa.**
* **Y A kétszemélyes játék modelljének állapotgráfjából kialakított irányított fa.**

### 12. Mely fogalmak kapcsolhatók egymáshoz a részleges játékfa-kiértékeléseknél?
* negamax algoritmus - könyebb implementáció
* (m,n) átlagoló kiértékelés - kiértékelő függvény...
* váltakozó mélységű kiértékelés  - megbízhatóbb
* alfa-béta algoritmus - hatákonyabb módszer


## 8

### 1. Milyen az általános vezérlési stratégiája az evolúviós algoritmusoknak?
* visszalépéses
* mohó
* gráfkereső
* **Y nem-módosítható**

### 2. Mit tárol az evolúciós algoritmus a globális munkaterületén?
* **Y A populációt.** 
* Az egyedek alkotta problémateret. 
* Az evolúciós operátorokat. 
* A rekombinációra kiválasztott egyedek halmazát. 

### 3. Melyik NEM evolúciós operátor az alábbiak közül?
* Rulett kerék algoritmus. 
* Kétpontos keresztezés.
* **Y Egy egyed kódolása.**
* Véletlen cseréje a kód két elemének. 

### 4. Hogyan szokták az egyedeket kódolni?
* Úgy, hogy a kódolás és a dekódolás is hatékony legyen.
* Úgy, hogy a dekódolás gyors legyen, mert a fittnesz függvényt az egyedre lehet kiszámolni.
* Úgy, hogy az egyed kódja egy kromoszóma legyen.
* **Y Úgy, hogy a kód darabjai az egyed egy-egy tulajdonságát mutassa.**

### 5. Hol épülhet véletlenített módszer az evolúciós algoritmusba?
* Csak a keresztezési pontok megadásában.
* **Y Csak a kezdeti populáció kialakításában és mind a négy evolúciós operátorban.** 
* Csak a populáció lecserélendő egyedeinek előállításában.
* Csak a kiválasztásban, a rekombinációban, és a mutációban. 

### 6. Hol van szerepe a kiválasztásnak az evolúciós algoritmusban? (3)
* A keresztezési pontok megadásában.
* **Y A rekombinációhoz szükséges szülő egyedek előállításában és az új populáció kialakításában.**
* **Y A populáció lecserélendő egyedeinek előállításában.**
* **Y Ez az első lépése az evolúciós ciklusnak.**

### 7. Mi a lényege a jó kiválasztási módszernek az evolúciós algoritmusokban?
* Megkeresi a populáció legjobb egyedét. 
* A fittnesz függvény alapján rendezi sorba a populáció egyedeit.
* **Y A rátermett egyedeket nagyobb valószínűséggel választja ki, de ad esélyt a kevésbé rátermettek kiválasztására is.** 
* Figyelembe veszi, hogy a kódban melyek az egyed tulajdonságait jelző szakaszok.

### 8. Mi a kapcsolat a keresztezés és a rekombináció között?
* **Y A keresztezések speciális rekombinációk.**
* A rekombináció a szülő egyedeken, míg a keresztezés azok kódjával dolgozik.
* A rekombinációk speciális keresztezések.
* A keresztezés mindig megelőzi a rekombinációt.

### 9. Melyek lehetnek a feltételei az evolúciós algoritmus leállásának? (2)
* **Y Célegyed megjelenése a populációban.**
* A populáció minden egyedének fittneszértéke meghalad egy adott korlátot.
* Nincsen a populációnak adott korlátnál nagyobb fittneszértékű egyede. 
* **Y A populáció összesített fittneszértéke már nem egy ideje nem változik.**


### 10. Mely keresztezési módszerek őrzik meg  permutáció tulajdonságot? (2)
* **Y Ciklikus keresztezés.**
* **Y Parciálisan illesztett keresztezés.**
* Egypontos keresztezés.
* Egyenletes keresztezés. 

### 11. Az alábbiak közül, melyek alkalmas módszerek a permutáció tulajdonságot megőrző mutációra? (2)
* Kód növekvő sorba rendezése. 
* **Y Kód egy szakaszának átrendezése**
* **Y Kód két véletlen választott elemének cseréje.**
* Kód első két elemének cseréje

### 12. Mely fogalmak kapcsolhatók egymáshoz az evolúciós algoritmusoknál?
* kétpontos keresztezés - parciállis illesztés
* kiválasztás - fittnes függvény
* egyed - kód
* stratégiai paraméter - populáció mérete

## 9

### 1. Mi az a rezolúciós gráf?
* **Az összes klóz előállítását bemutató gráf.**
* Logikai következtetést szimbolizáló ÉS/VAGY gráf. 
* Az útkeresési feladatot leíró irányított gráf.
* Az üres klóz előállítását bemutató gráf.

### 2. Melyek a p || q   és a   !p || !q  rezolvensei?
* nem rezolválhatók
* üres klóz
* **p || !p    és   q || !q**
* p || q || !q     és    q || p || !p

### 3. Mi a globális munkaterülete a rezolúciónak?
* Az egyedek populációja.
* Az axiómákból és a célállítás negáltjából kialakított klózok halmaza. 
* **A kiinduló és az eddig előállított klózok halmaza.**
* A formalizációban részt vevő predikátumok halmaza. 

### 4. Mi a keresési szabálya a rezolúciónak?
* Az üres klóz előállítása.
* Az üres klóz levezetése. 
* **rezolvens képzés.**
* A Skolemizálás. 

### 5. Melyik az alábbiak közül a visszafelé haladó szabályalapú reprezentáció jellemzője?
* A célállítás egy L1 || … || Ln egzisztenciálisan kvantált formula, ahol Li literál.
* A szabályok L→W alakúak, ahol W egy ÉS/VAGY formula, L egy literál, és minden változó univerzálisan kvantált.
* **A szabályok W→L alakúak, ahol W egy ÉS/VAGY formula, L egy literál, és minden változó univerzálisan kvantált.**
* A tényállítás egy univerzálisan kvantált ÉS/VAGY formula. 


### 6. Melyik az alábbiak közül az előrefelé haladó szabályalapú reprezentáció jellemzője?
* A célállítás egy egzisztenciálisan kvantált ÉS/VAGY formula.
* A tényállítás egy L1 || … || Ln univerzálisan kvantált formula, ahol Li literál.
* **A szabályok L→W alakúak, ahol W egy ÉS/VAGY formula, L egy literál, és minden változó univerzálisan kvantált.**
* A szabályok W→L alakúak, ahol W egy ÉS/VAGY formula, L egy literál, és minden változó univerzálisan kvantált.

### 7. Hogyan kell a rezolúciót válaszadásra felhasználni?
* A rezolúció csak igen/nem jellegű választ képes adni.
* Az A1, … , An ⟹ C kérdés helyett az A1 && … && An && !C kielégíthetetlenségét vizsgáljuk.
* A kérdésre adható választ egy külön predikátummal jelenítjük meg a célállításban.
* **A választ egy egzisztenciálisan kvantált változóval kell megjeleníteni a célállításban.**

### 8. Mi következik abból, hogy a rezolúció módszere helyes? (2)
* **Ha elakad (nem tud újabb klózt előállítani), akkor a kiinduló klózhalmaz kielégíthető.**
* **Ha üres klózzal terminál, akkor a kiinduló klózhalmaz kielégíthetetlen.**
* Mindig elő tudja állítani az üres klózt. 
* Kicsi a futási ideje. 


### 9. Mi következik abból, hogy a rezolúció módszere teljes? (2)
* **Ha a kiinduló klózhalmaz kielégíthető, akkor nem állítja elő az üres klózt.**
* **Ha a kiinduló klózhalmaz kielégíthetetlen, akkor levezethető az üres klóz**
* Minden A1, … , An ⟹ C alakú tétel bizonyítására vagy cáfolására alkalmas.
* Ha a kiinduló klózhalmaz kielégíthetetlen, akkor véges lépésen belül terminál.

### 10. Melyek az alábbiak közül a rezolúció reprezentációs gráfjának különös tulajdonságai? (2) 
* **Ha a stratcsúcsból vezet út célcsúcsba, akkor mindegyik startcsúcsból elérhető csúcsból is vezet célcsúcsba út.**
* **Nincs benne kör.**
* Nincs benne zsákutca.
* Bármelyik csúcsból bármelyik csúcsba el lehet jutni.

### 11. Melyek lehetnek az alábbiak közül a rezolúció modellfüggő vágó stratégiái? (2)
* **Minden rezolúciós lépésben az egyik szülőklóz az utoljára előállított klóz legyen.**
* **Minden rezolúciós lépésben az egyik szülőklóz egyetlen literálból álljon.**
* Mindig azt a klózpárt rezolváljuk előbb, amelyikben a literálok száma a legkevesebb.
* Soroljuk be szintekre a rezolúciós gráf klózait. Nulladik szinten a kiinduló klózok, az i+1-dik szinten azok, amelyek egyik szülője az i-dik szinten van, másik szülője az első i szint valamelyikén. Állítsuk elő szintenként a klózokat.

### 12. Melyek az alábbiak közül a rezolúció modellfüggő sorrendi stratégiái? (2)
* **Soroljuk be szintekre a rezolúciós gráf klózait. Nulladik szinten a kiinduló klózok, az i+1-dik szinten azok, amelyek egyik szülője az i-dik szinten van, a másik szülő az első i szint valamelyikén. Állítsuk elő szintenként a klózokat.**
* Minden rezolúciós lépésben az egyik szülőklóz az utoljára előállított klóz legyen. 
* **Mindig azt a klózpárt rezolváljuk, amelyekben a literálok száma a legkevesebb.**
* Minden rezolúciós lépésben az egyik szülőklóz egyetlen literálból álljon. 


## 10

### 1. Hogyan számoljuk az A esemény valószínűségét feltéve, hogy B esemény – amely valószínűsége nagyobb, mint nulla – bekövetkezik?
* P(A|B) = P(A)P(B) / P(B) 
* P(A|B) = P(B|A)P(B) / P(A) 
* P(A|B) = P(A,B) / P(A) 
* **P(A|B) = P(A,B) / P(B)**


### 2. Mikor mondjuk, hogy A és B események feltételesen függetlenek E eseményre nézve?
* **P(AB|E) = P(A|E) P(B|E)**
* P(AB|E) = P(A|E) P(B|E) / P(E) 
* P(AB|E) = P(B|E) 
* P(AB|E) = P(A|E) 

### 3. Az alábbiak közül melyik egy Bayes tétel?
* **P(A|B) = P(B|A) P(A) / P(B)**
* P(B|A,E) = P(A,B|E) P(A|E) / P(B|E) 
* P(B|A,E) = P(A|B,E) P(A|E) / P(B|E)
* P(A|B) = P(B|A) P(B) / P(A)

### 4. Az alábbiak közül melyik NEM igényel bizonytalanság kezelést?
* **Axiómákból kiinduló logikai következtetés.**
* Ellentmondó adatokra épülő következtetés. 
* Hiányzó adatok alapján történő következtetés. 
* Elmosódott jelentésű állítások alapján történő következtetés.

### 5. Milyen gráf a valószínűségi háló?
* 𝛿-gráf. 
* **Véges körmentes irányított gráf.**
* Véges fa. 
* Véges fa-gráf. 

### 6. Mit mutat meg a valószínűségi háló feltételes valószínűségi táblája?
* Azt, hogy egy csúcs valószínűségi változója milyen valószínűséggel vesz fel egy adott értéket feltéve, hogy a gyerek csúcsok valószínűségi változói adott értékűek.
* Azt, hogy egy él valószínűségi változója milyen valószínűséggel vesz fel egy adott értéket feltéve, hogy az él végcsúcsából kifutó élek valószínűségi változói adott értékűek.
* **Azt, hogy egy csúcs valószínűségi változója milyen valószínűséggel vesz fel egy adott értéket feltéve, hogy a szülő csúcsok valószínűségi változói adott értékűek.**
* Azt, hogy egy él valószínűségi változója milyen valószínűséggel vesz fel egy adott értéket feltéve, hogy az él kezdőcsúcsába futó élek valószínűségi változói adott értékűek.

### 7. Mit jelent a normalizálás technikája? (2)
* **Adott összegű kifejezések közös együtthatójának kiszámolását.**
* Bayes hálók fa-gráfokká történő átalakítását. 
* **Adott kifejezések olyan együtthatóval történő szorzását, hogy ezáltal az összegük 1 legyen.**
* A kettes norma alkalmazását.

### 8. Mit jelent az, hogy egy valószínűsági háló egyszeresen kötött?
* Azt, hogy a háló éleinek irányításait megfordítva irányított fát kapunk. 
* Azt, hogy a háló egy irányított fa. 
* Azt, hogy a háló körmentes. 
* **Azt, hogy a háló egy fa-gráf.**

### 9. Az alábbiak közül melyek igazak a valószínűségi hálókra? (2)
* **Irányított élei a válószínűségi változók közötti közvetlen ok-okozati összefüggéseket mutatják.**
* **Csúcsai egy adott tárgykör valószínűségi változóit reprezentálják.**
* Az éleiről elhagyva az irányítást a hálóból egy irányítatlan fát kapunk.
* Egyetlen célcsúcsa van. 

### 10. Hogyan javítható a valószínűségi hálóban való számítás hatékonysága, ha a háló nem fa-gráf? (3)
* **Csúcsok összevonásával fa-gráffá alakítjuk a valószínűségi hálót.**
* **A valószínűségi hálót példák generálására használjuk, amelyekből relatív gyakoriságot számolunk.**
* **Csúcsok elhagyásával több fa-gráfokra bontjuk a valószínűségi hálót.**
* Nem javítható. 

### 11. Milyen heurisztikus bizonytalanságkezelő technikákról hallott? (2)
* Zárt világ feltételezés.
* **Fuzzy következtetés.**
* **MYCIN szakértő rendszer következtetése.**
* Bayes-i frissítés módszere. 

### 12. Mely fogalmak kapcsolhatók egymáshoz a bizonytalanság kezelésnél?
* fa-gráf - csúcsok összevonása
* heurisztikus módszer - MYCIN
* bizonytalan következmény - feltételes valószínűség
* valószínűségi háló - véges körmentes

## 11

### 1. Mit jelent az, hogy egy tanulás felügyelt?
* A tanulási folyamatnak ki kell számolni a tanító minták elvárt kimenetét is.
* A tanulás folyamatát módosítani kell, ha az elvárt kimenet eltér a számítottól.
* **A tanító minták elvárt kimenetét is felhasználja a tanulási folyamat.**
* A tanulás folyamata nem teljesen automatikus. 

### 2. Mit jelent az, hogy egy tanulás felügyelet nélküli?
* **A tanulásnak nincs szüksége a tanító minták elvárt kimenetére.**
* A tanító mintákra kiszámolt kimenet eltérhet az elvárt kimenettől. 
* A tanulás folyamata teljesen automatikus. 
* A tanító minták elvárt kimenetét automatikusan számolja a tanulás módszere.

### 3. Mit jelent a zaj a tanító minták esetén?
* Amikor a tanítóminták elvárt kimenetének jelentése elmosódott.
* **Amikor azonos attribútumokkal rendelkező minták eltérő elvárt kimenetekkel rendelkeznek.**
* Amikor a tanítóminták elvárt kimenete hasonló. 
* Amikor két vagy több eltérő attribútumokkal rendelkező minta elvárt kimenetei megegyeznek.


### 4. Különböző tanító minták halmazának mikor a legkisebb az információ (entrópia) tartalma a döntési fáknál?
* Ha a minták kimeneti értékei közötti legnagyobb távolság (valamilyen távolság metrika mellett) kisebb a legnagyobb kimeneti értéknél (ugyanazon metrika szerint). 
* Ha a kimeneteik értékei mind különböznek. 
* Ha a minták inputjai közötti legnagyobb távolság (valamilyen távolság metrika mellett) kisebb a legnagyobb input értéknél (ugyanazon metrika szerint).
* **Ha mind azonos kimeneti értékkel rendelkezik.**


### 5. Hogyan értékelünk ki a döntési fa építése során egy levélcsúcsot akkor, ha nem tartoznak hozzá tanító minták?
* **A szülőcsúcsához tartozó tanítóminták alapján.**
* Ilyen eset nem fordulhat elő. 
* A szülőcsúcsához tartozó attribútumok alapján. 
* A csúcshoz tartozó attribútumok alapján, ha vannak ilyenek, különben véletlenszerű értéket kap. 


### 6. A döntési fa építése során az alábbiak közül milyen csúcsok fordulhatnak elő a fában? (3)
* **Attribútummal címkézett belső csúcsok.**
* Attribútummal címkézett levél csúcsok. 
* **Kiértékeletlen levélcsúcsok.**
* **Kiértékelt levélcsúcsok**


### 7. Mely állítások igazak a döntési fára? (2)
* Gyökércsúcsa a kiinduló problémát reprezentálja. 
* **Egy csúcsból kivezető élei a csúcs attribútumának lehetséges értékeit szimbolizálják.**
* **Belső csúcsai egy-egy attribútumot reprezentálnak.**
* Ágai egy probléma lehetséges megoldását adják. 

### 8. Mely állítások igazak a döntési fa módszerére? (2)
* **A válaszadási idő rövid.** 
* Optimális megoldást ad. 
* **A tanulási idő hosszú.**
* A mintákat a válaszadásnál is ismerni kell. 


### 9 Mely állítások igazak k-legközelebbi szomszéd módszerére? (2)
* **A megtanult paraméter a minták összessége.**
* A tanulási idő hosszú. 
* A válaszadási idő rövid. 
* **Egyszerű implementálni.**


### 10. Milyen felügyelt tanulási módszereket ismert meg a kurzuson? (3)
* **k-legközelebbi szomszéd módszere.**
* **Véletlen erdő módszere.**
* **Error backpropagation algoritmus.**
* k-közép módszer


### 11. Hol jutott szerepe a véletlennek a véletlen erdő módszerében? (2)
* **Az erdő egy fájának felépítéséhez a minták attribútumai közül véletlen választott attribútumokat használ.**
* A fa egy csúcsához rendelt attribútumot véletlen módon választja ki. 
* **Az erdő egy fájának felépítéséhez a minták véletlen választott részhalmazát használja.**
* Az erdő fáinak számát véletlen módon határozzák meg.

### 12. Mely fogalmak kapcsolhatók össze az alábbiak közül a gépi tanulás témakörében?
* felügyelt tanulás - tanító minták elvárt kiemelése
* kerszt entrópia - 2-es norma
* döntési fa - véletlen erdő
* k-közép módszer - osztályozási feladat

## 12

### 1. Az alábbiak közül melyik jellemzik a homogén többrétegű előrecsatolt hálózatot?
* A különböző rétegek neuronjainak aktivációs (kimeneti) függvénye eltérhet, de egy réteghez tartozó neuronok esetében nem. 
* **Az azonos réteghez tartozó neuronok között nincs közvetlen kapcsolat.**
* Az i-edik réteg egy neuronjának kimenete csak az i-1-dik réteg neuronjának lehet bemeneti értéke.
* **Az i-dik réteg neuronjának kimenete csak az i+1-dik réteg neuronjának lehet bemeneti értéke.**


### 2. Mit jelent az input vektorizálása?
* **Egy inputot a jellemzői (attribútumai) segítségével egy számsorozattal ábrázolunk.**
* A megoldandó probléma lineárisan szeparálható feladattá konvertálását.
* Az inputok azonos hosszúságú számsorozatok. 
* Az inputot egy síkvektorként fogjuk fel, amelynek kiinduló pontja az origó.


### 3 Jellemezze a szigmoid kimeneti függvényt!
* Folytonos, mindenhol deriválható, monoton növekedő, [0,1] intervallumba képző függvény.
* Folytonos, majdnem mindenhol deriválható, monoton növekedő, ]0,1[ intervallumba képző függvény.
* Egyetlen szakadási ponttal rendelkező, máshol deriválható, monoton növekedő, [0,1] intervallumba képző függvény.
* **Folytonos, mindenhol deriválható, szigorúan monoton növekedő, ]0,1[ intervallumba képző függvény.**

### 4. Az alábbiak közül melyik hálózatnak NEM lehet több rétegű topológiája?
* Backpropagation modell hálózata.
* Rekurrens neurális hálózat. 
* **Hopfield neurális hálózat.**
* Konvolúciós neurális hálózat. 


### 5 Mi a delta tanulási szabály?
* Egy súly megváltoztatása a súlyhoz tartozó bemeneti értéknek, és a súlyt tartalmazó neuron várt kimeneti értékének szorzatától függ.
* Egy súly megváltoztatása a súlyhoz tartozó bemeneti értéknek, és a súlynak szorzatától függ.
* Egy súly megváltoztatása a súlyhoz tartozó bemeneti értéknek, és a súlyt tartalmazó neuron számított kimeneti értékének szorzatától függ.
* **Egy súly megváltoztatása a súlyhoz tartozó bemeneti értéknek, és a súlyt tartalmazó neuron számított és várt kimeneti értékei különbségének szorzatától függ.**


### 6 Mire alkalmazzák a lineárisan szeparálható kifejezést?
* Arra, hogy a mintapontokhoz a legkisebb négyzetek módszerével meghatározott egyenes elválasztja egymástól a mintapontokat.
* Arra, hogy a perceptronnal megoldható problémák két osztályba sorolhatóak be. 
* A Rosenblatt-féle perceptronokból épített neurális hálózatokra. 
* **Azokra a feladatokra, amelyek lehetséges bemeneti érték n-esei egy hipersíkkal elválaszthatók aszerint, hogy az ezekre elvárt válasz A vagy B.**


### 7 A mesterséges neuron hálózatokra felügyelt vagy felügyelet nélküli tanulási módszer alkalmazható?
* Egyik sem. 
* Csak felügyelt.
* **Mindkettő.**
* Csak felügyelet nélkül.


### 8 Hogyan lehet Rosenblatt-féle perceptronok felhasználásával koordinátapárokat úgy osztályozni, hogy megmondjuk melyek esnek bele egy megadott háromszögbe, és melyek nem?
* Nem lehet, mert többrétegű Rosenblatt-féle perceptronokból álló hálózathoz nem ismerünk tanuló algoritmust. 
* **Olyan kétrétegű előrecsatolt hálózattal, ahol az első rétegben három, a második rétegben egy neuron van.**
* Nem lehet, mert a Rosenblatt-féle neuronokkal csak lineárisan szeparálható problémákat lehet megoldani. 
* Egy rétegű három neuront tartalmazó hálózattal. 

### 9 A mesterséges neuronhálózatot egy olyan paraméteres függvénynek tekinthetjük, amellyel a megoldandó problémát reprezentáló leképezést közelítjük. Melyek ebben a paraméterek?
* **A neuronok súlytényzői.**
* A neuronok „bias” bemenete. 
* A neuronokban használt kimeneti függvények.
* A tanító minták száma és a tanulási együttható.


### 10 Mit értünk a hiba-visszaterjesztés (error-backpropagation) módszere alatt?
* Olyan többrétegű hálózat építését, amelyben megengedjük a visszacsatolást a szomszédos rétegek között.
* **Azt, amikor egy többrétegű előrecsatolt hálózat kimeneti rétegének számított és várt outputjai alapján határozzuk meg, hogy hogyan kell a hálóbeli neuronok súlyait változtatni.**
* Azt a folyamatot, amellyel a Hopfield modell stabil konfigurációba jut.
* Azt, amikor egy többrétegű előrecsatolt hálózat kimeneti rétegének elvárt kimenetei alapján határozzuk meg, hogy a hálóbeli neuronoknak milyen elvárt kimenete van. 


### 11 Mit értünk a Hopfield modell konfigurációs terén?
* A neuronok bemeneteinek összességét. 
* A neuronok súlyainak összességét.
* **A neuronok kimeneteinek összességét.** 
* **A neuronok által felvett állapotok összességét.** 


### 12 Az alábbiak közül mely állítások igazak a mesterséges neuronhálózatokra? (2)
* A mintákat egyesével el kell tárolni
* **A tanulási idő hosszú.**
* **A válaszadási idő rövid.**
* Optimális megoldást ad. 