<!-- extends layouts/mdLayout.pug -->
 
<!-- block title -->
<!-- ++title("Telkom ZH helper") -->

<!-- block mdContent -->

# Telekommunikációs hálózatok ZH segédlet

## Socket

### Connect to server

```python
import socket

# Create TCP Socket
tcp_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp_sock.connect((SERVER_IP, SERVER_PORT))

# Send data
tcp_sock.sendall(b'data')

# Receive data
tcp_sock.recv(1024)

# Close the connection
tcp_sock.close()
```

```python
import socket

# Create UDP Socket
udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

udp_sock.sendto(b'data', (IP, PORT))

# No connection, nothing to close

```

### Socket server

```python
# TCP Version
import select
import socket
import sys
import struct

class SimpleTCPSelectServer:
    def __init__(self, addr='localhost', port=10001, timeout=1):
        self.server = self.setupServer(addr, port)
        # Sockets from which we expect to read
        self.inputs = [self.server]
        # Wait for at least one of the sockets to be ready for processing
        self.timeout = timeout
        self.packer = struct.Struct("ci")

    def setupServer(self, addr, port):
        # Create a TCP/IP socket
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setblocking(0)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Bind the socket to the port
        server_address = (addr, port)
        server.bind(server_address)

        # Listen for incoming connections
        server.listen(5)
        return server

    def handleNewConnection(self, sock):
        # A "readable" server socket is ready to accept a connection
        connection, client_address = sock.accept()
        connection.setblocking(0)  # or connection.settimeout(1.0)
        self.inputs.append(connection)

    def handleDataFromClient(self, sock):
        raw_data = sock.recv(1024)
        if raw_data:
            pass # Put your logic here, handle incomming user data
            
          
        else:
            # Interpret empty result as closed connection
            # Stop listening for input on the connection
            self.inputs.remove(sock)
            sock.close()

    def handleInputs(self, readable):
        for sock in readable:
            if sock is self.server:
                self.handleNewConnection(sock)
            else:
                self.handleDataFromClient(sock)

    def handleExceptionalCondition(self, exceptional):
        for sock in exceptional:
            # Stop listening for input on the connection
            self.inputs.remove(sock)
            sock.close()

    def handleConnections(self):
        while self.inputs:
            try:
                readable, writable, exceptional = select.select(
                    self.inputs, [], self.inputs, self.timeout)

                if not (readable or writable or exceptional):
                    continue

                self.handleInputs(readable)
                self.handleExceptionalCondition(exceptional)
            except KeyboardInterrupt:
                # print("A szerver leáll")
                for c in self.inputs:
                    c.close()
                self.inputs = []
```

```python
# UDP Version

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((IP, PORT))
sock.settimeout(1.0)

# Receive data
data, address = sock.recvfrom(1024) # Address is a tuple of IP and PORT

# Send data
sock.sendto(data, address)

# Close conncetion
sock.close()
```

## Checksum and crc

```python
import hashlib

# Calculate MD5 hash
h = hashlib.md5('The content you want to hash')

# The size of the hash
h.digest_size

# To get the hash
h.digest()
h.hexdigest()
```

## Struct


Formattign cheat sheet [here](https://gist.github.com/vpayno/6858645#format-string)


```python

import struct

# Create a packer
unpacker = struct.Struct('I I I I 6s')

# Unpack data
data = unpacker.unpack(raw_data)

# Pack data
raw_data = unpacker.pack(data)

# Get the size
calcsize("formatstring")
unpacker.size
```